require_relative '../rover'


describe 'Rover' do
  it 'has an initial position' do
    x_coord = 1
    y_coord = 5
    orientation = "N"
    rover = Rover.new(x_coord: x_coord, y_coord: y_coord, orientation: orientation)

    initial_position = rover.position

    expect(initial_position).to eq({ x_coord: x_coord, y_coord: y_coord, orientation: orientation })
  end

  it 'turns left' do
    x_coord = 1
    y_coord = 5
    orientation = "N"
    rover = Rover.new(x_coord: x_coord, y_coord: y_coord, orientation: orientation)

    rover.move("L")

    expect(rover.position).to eq({ x_coord: x_coord, y_coord: y_coord, orientation: "W" })
  end

  it 'turns right' do
    x_coord = 1
    y_coord = 5
    orientation = "N"
    rover = Rover.new(x_coord: x_coord, y_coord: y_coord, orientation: orientation)

    rover.move("R")

    expect(rover.position).to eq({ x_coord: x_coord, y_coord: y_coord, orientation: "E" })
  end
end
