
class Rover
  EAST = "E"
  WEST = "W"
  LEFT = "L"

  def initialize(x_coord:, y_coord:, orientation:)
    @x = x_coord
    @y = y_coord
    @orientation = orientation
  end

  def position
    {x_coord: @x, y_coord: @y, orientation: @orientation}
  end

  def move(movements)
    @orientation = EAST
    @orientation = WEST if turn_left?(movements)
  end

  def turn_left?(movements)
    movements == LEFT
  end

end
